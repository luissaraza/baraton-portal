# El Baratón Portal


Don Pepe wants to expand his services and offer his clients a vanguard way of acquiring their products.

Therefore, an e-commerce portal was created for the sale and distribution of products from the store "El Baratón"

## Problem resolution

**Tech specs**

 * Angular 6
 * Typescript
 * Sass
 * BEM Notation (Css)
 
 
 Taking into account the size of the market, and the proposed requirements, a scalable lightweight architecture was built, therefore, a SPA was created that fulfilled the client's objectives, encapsulating everything in an organized, modular and structured way.
 
 The different modules that are part of the application were created separately, making them reusable, maintainable and scalable.
 
 For the persistence of data, `IndexDB was used, used in all browsers as the best method to maintain large and complex blocks of information in session.
 
 The layout was developed based on the material concept, implementing the Angular Material library, the main strategy of style mapping was the `BEM methodology`, which allows to identify the blocks, elements and modifiers in a simpler way, giving a better sense to HTML architecture in general
 
 

## Set up - steps

* Clone the repo: `git clone https://gitlab.com/luissaraza/baraton-portal.git`

* Run `npm install`

**Run simulated backend:**

* Simulate a backend side  with `JSON server`: https://github.com/typicode/json-server
 
* Run `npm run mock-server` 

**Run the project (In a new Terminal):**

* Run `npm start` for a dev server.
 
* Navigate to `http://localhost:4200/`.
