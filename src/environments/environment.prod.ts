export const environment = {
  production: true,
  serverUrl: 'http://localhost:3000',
  defaultLanguage: 'Español',
  supportedLanguages: [
    'English',
    'Español'
  ],
};