import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

import { Category } from '@app/core/models/category.model';
import { Product } from '@app/core/models/product.model';

import { StoreService } from '@app/shared/services/store/store.service';

import { Subscription } from 'rxjs';

interface LoadingDashboard {
  categories: boolean;
  products: boolean;
}

@Component({
  selector: 'app-home',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.scss']
})

/**
 * This component will handle the categories and products components and will be the first view
 */
export class DashboardComponent implements OnInit {
  public productsRequest: Subscription;
  public products: Product[];
  public categories: Category[];
  public currentCategory: Category;
  public loadingControl: LoadingDashboard;

  constructor(private router: Router, private storeService: StoreService) {
    this.loadingControl = {
      categories: true,
      products: false,
    };

    this.currentCategory = null;
  }

  ngOnInit() {
    // First request of categories to build the categories list
    this.storeService.getCategories().subscribe(categories => {
      this.categories = categories;
      this.loadingControl.categories = false;
    });
  }

  /**
   * This Method will handle when the user selects a category from the category list component
   * Search the products from category id, if the category has been loaded, then show the current products
   */
  onSelectedCategory(category: Category) {
    // Check if the category has been loaded before
    const loadedCategory = this.storeService.loadedCategories.find(catPro => catPro.id === category.id);

    // Cancel previous request this has not been finalized
    if (this.productsRequest) { this.productsRequest.unsubscribe(); }
    this.currentCategory = category;

    if (loadedCategory) {
      this.products = loadedCategory.products;
      this.loadingControl.products = false;
    } else {
      this.loadingControl.products = true;

      // When is a new category, then request the products from category and load the products list component
      this.productsRequest = this.storeService.getProducts(category).subscribe(products => {
        this.products = products;
        this.loadingControl.products = false;
      });
    }
  }
}
