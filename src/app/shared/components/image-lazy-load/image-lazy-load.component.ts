import { Component, Input, OnInit } from '@angular/core';

@Component({
  selector: 'app-image-lazy-load',
  templateUrl: './image-lazy-load.component.html',
  styleUrls: ['./image-lazy-load.component.scss']
})

/**
 * This will create lazy load image component to create a spinner element whiles the image is loaded
 */
export class ImageLazyLoadComponent implements OnInit {
  @Input() fromCard: boolean;
  @Input() customClass: string;

  @Input()
  set src(valueSrc: string) {
    this.watchImage(valueSrc)
      .then(() => {
        this._imageSrc = valueSrc;
        this.loaded = true;
      })
      .catch(() => {
        this.loaded = false;
        this.error = true;
      });
  }

  get src(): string {
    return this._imageSrc;
  }

  public loaded: boolean;
  public error: boolean;

  private _imageSrc: string;

  constructor() {
    this.loaded = false;
    this.error = false;
  }

  ngOnInit() {
  }

  /**
   * Method to watch when image loads
   */
  private watchImage(src: string): Promise<void> {
    return new Promise((resolve, reject) => {
      const img = new Image();
      img.onload = () => resolve();
      img.onerror = () => reject();

      img.src = src;
    });
  }
}
