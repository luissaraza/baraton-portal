import { Component, forwardRef, Input } from '@angular/core';
import { ControlValueAccessor, NG_VALUE_ACCESSOR } from '@angular/forms';

const customValueProvider = {
  provide: NG_VALUE_ACCESSOR,
  useExisting: forwardRef(() => RangeFilterComponent),
  multi: true
};

@Component({
  selector: 'app-range-filter',
  templateUrl: './range-filter.component.html',
  styleUrls: ['./range-filter.component.scss'],
  providers: [customValueProvider]
})

/**
 * This will create a component to filter show a range of numbers on inputs, use controlValueAccessor to control the ngModel option
 */
export class RangeFilterComponent implements ControlValueAccessor {

  // Set accessor including call the onchange callback
  @Input()
  set currentModel(modelValue: any) {
    if (modelValue !== this._modelValue) {
      this._modelValue = modelValue;
      this._propagateChange(modelValue);
    }
  }

  // Get accessor
  get currentModel(): any {
    return this._modelValue;
  }

  private _modelValue: any;
  private _propagateChange: (model: any) => void;

  constructor() { }

  // From ControlValueAccessor interface
  writeValue(model: any) {
    if (model) {
      this._modelValue = model;
    }
  }

  // From ControlValueAccessor interface
  registerOnChange(fn) {
    this._propagateChange = fn;
  }

  // From ControlValueAccessor interface
  registerOnTouched(fn: () => void): void {
    //
  }

  /**
   * Method to avoid the user to write other than numbers
   */
  public onlyNumberKey(event: KeyboardEvent): boolean {
    return (event.keyCode === 8 || event.keyCode === 0) ? null : event.keyCode >= 48 && event.keyCode <= 57;
  }

}
