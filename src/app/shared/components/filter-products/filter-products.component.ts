import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { animate, state, style, transition, trigger } from '@angular/animations';

import { Product } from '@app/core/models/product.model';
import { Filter, MinMax } from '@app/core/models/filter.model';

@Component({
  selector: 'app-filter-products',
  templateUrl: './filter-products.component.html',
  styleUrls: ['./filter-products.component.scss'],
  animations: [
    trigger('showHideAnimation', [
      state('show', style({
        height: '80px'
      })),
      state('hide', style({
        height: '0',
        padding: '0',
        opacity: '0',
        pointerEvents: 'none'
      })),
      transition('hide => show', animate('300ms ease')),
      transition('show => hide', animate('300ms ease'))
    ])
  ]
})

/**
 * This will create a component to filter the products listed
 */
export class FilterProductsComponent implements OnInit {

  @Input() set products(prods: Product[]) {
    if (prods) {
      this._originalProducts = prods;
    }
  }

  @Output() applied: EventEmitter<Product[]> = new EventEmitter();
  @Output() reseted: EventEmitter<void> = new EventEmitter();

  public hideFilter: boolean;
  public filtersModel: Filter;

  private _originalProducts: Product[];

  constructor() {
    this.hideFilter = true;
    this.initFiltersModel();
  }

  ngOnInit() {}

  /**
   * Method to make a toggle on the filter component, show or hide the component
   * @param  hide - Set value to force hide or show
   * @returns  Returns the current hideFilters value if we want to get it
   */
  public toggleFilter(hide?: boolean): boolean {
    this.hideFilter = hide || !this.hideFilter;
    return this.hideFilter;
  }


  /**
   * Method to emit apply event to components that use this one
   */
  public apply() {
    const originalProd: Product[] = [...this._originalProducts];
    let tempProducts: Product[] = originalProd;

    // Filter by available
    if (this.filtersModel.available !== null) {
      tempProducts = originalProd.filter(pro => pro.available === this.filtersModel.available);
    }

    // then filter by price and quantity
    tempProducts = this.filterRangeFn(this.filtersModel.priceRange, tempProducts, 'formattedPrice');
    tempProducts = this.filterRangeFn(this.filtersModel.qtyRange, tempProducts, 'quantity');

    // Emit the filtered products
    this.applied.emit(tempProducts);
  }

  /**
   * Method to emit reset event to components that use this component
   * Reset date component and filterList(Mobile) components
   */
  public reset() {
    this.initFiltersModel();
    this.reseted.emit();
  }

  /**
   * This method will filter an array of products by range of values
   */
  private filterRangeFn(filterRange: MinMax , products: Product[], index: string): Product[] {
    const [min, max] = [filterRange.min, filterRange.max];
    if (!min && !max) { return products; }

    return products.filter( pro => {
      return min && max ?
        pro[index] >= min && pro[index] <= max : min ?
          pro[index] >= min : pro[index] <= max;
    });
  }

  /**
   * Method to init and reset filters model
   */
  private initFiltersModel() {
    this.filtersModel = {
      available: null,
      priceRange: {
        min: null,
        max: null,
      },
      qtyRange: {
        min: null,
        max: null,
      },
    };
  }
}
