import { Component, OnInit, ViewChild } from '@angular/core';
import { animate, style, transition, trigger } from '@angular/animations';
import { MatSidenav, MatSnackBar } from '@angular/material';

import { Product } from '@app/core/models/product.model';

import { StoreService } from '@app/shared/services/store/store.service';
import { IndexDbService } from '@app/shared/services/indexDB/index-db.service';

interface Confirm {
  hasResponse: boolean;
  hasError: boolean;
  confirming: boolean;
  message: string;
  icon: string;
}

@Component({
  selector: 'app-shopping-cart',
  templateUrl: './shopping-cart.component.html',
  styleUrls: ['./shopping-cart.component.scss'],
  animations: [
    trigger(
      'enterAnimation', [
        transition(':enter', [
          style({ transform: 'translateX(100%)' }),
          animate('200ms', style({ transform: 'translateX(0)' }))
        ]),
        transition(':leave', [
          style({ transform: 'translateX(0)' }),
          animate('200ms', style({ transform: 'translateX(100%)' }))
        ])
      ]
    )
  ]
})

/**
 * This will create a shopping cart component to keep the products selected in one place and confirm the order
 */
export class ShoppingCartComponent implements OnInit {
  @ViewChild('sidenav') sidenav: MatSidenav;

  public products: Product[];
  public lastProduct: Product;
  public showCart: boolean;

  public confirmControl: Confirm;

  constructor(
    public snackBar: MatSnackBar,
    private storeService: StoreService,
    private indexDBService: IndexDbService,
  ) {
    this.showCart = false;

    this.confirmControl = {
      hasResponse: false,
      hasError: false,
      confirming: false,
      message:  '',
      icon: ''
    };
  }


  ngOnInit() {
    this.products = [];

    /**
     * This subject will watch when a product or products are updated (Added or removed), and create a snackBar for feedback the user
     * And update the current products and the IndexDb products store
     */
    this.storeService.onUpdateProduct$.subscribe(updateValue => {
      if (updateValue) {
        if (updateValue.add) {
          const actionText = this.lastProduct && this.lastProduct.id === updateValue.last.id ? ' Actualizado' : 'Agregado';
          this.lastProduct = updateValue.last;
          const priceText = `precio actual: ${this.lastProduct.current_price} `;
          const qtyText = `(${this.lastProduct.current_quantity} unidad${this.lastProduct.current_quantity !== 1 ? 'es' : ''})`;
          const snackText = `${this.lastProduct.name} ha sido ${actionText}, ${priceText} ${qtyText}`;

          this.snackBar.open(snackText, 'OK', {duration: 2000, verticalPosition: 'top'});
        }

        this.products  = updateValue.products;

        if (updateValue.last !== null) { this.updateIndexDB(this.products); }
      }
    });
  }

  /**
   * This method will remove a specific product of the store of products
   */
  public removeProduct(product: Product) {
    this.storeService.removeProduct(product);
  }

  /**
   * This method will confirm (Fake) the order, and update the confirmControl structure
   */
  public confirmOrder() {
    if (this.confirmControl.confirming) { return; }

    this.confirmControl.hasError = false;
    this.confirmControl.hasResponse = false;
    this.confirmControl.confirming = true;

    // Request the confirm method from store (On Dev will be a fake method with  a timeout delay)
    this.storeService.confirmOrderFake()
      .then(() => {
        this.confirmConfig(this.confirmControl, 'La orden ha sido confirmada', 'check_circle_outline');
        this.storeService.cleanOrder();
      })
      .catch(() => {
        this.confirmControl.hasError = true;
        this.confirmConfig(this.confirmControl, 'La orden ha sido rechazada, intentalo de nuevo', 'error_outline');
      });
  }

  /**
   * This method will toggle the shopping cart view;
   */
  public toggle(state?: boolean): boolean {
    if (this.confirmControl.confirming) { return; }
    this.showCart = state || !this.showCart;
  }

  public getTotalPrice() {
    if (this.products) {
      return this.products.map(pro => pro.current_price).reduce((totalValue, currentValue) => totalValue + currentValue , 0);
    }
  }


  /**
   * Method to standardize the confirm structure config
   */
  private confirmConfig(confirm: any, message: string, icon: string) {
    confirm.hasResponse = true;
    confirm.message = message;
    confirm.icon = icon;

    setTimeout(() => {
      confirm.confirming = false;
    }, 2000);
  }

  /**
   * Method to update the category-products store with a list of products
   */
  private updateIndexDB (currentProducts: Product[]) {
    this.indexDBService
      .openDB('PortalBaraton')
      .set('category-products', 'products', currentProducts);
  }
}
