import { Component, Input } from '@angular/core';

import {Product} from '@app/core/models/product.model';

import { StoreService } from '@app/shared/services/store/store.service';
import {TranslateService} from '@ngx-translate/core';

enum SortFilterEnum {
  price = 'formattedPrice',
  available = 'available',
  qty = 'quantity',
}

@Component({
  selector: 'app-products-list',
  templateUrl: './products-list.component.html',
  styleUrls: ['./products-list.component.scss']
})

/**
 * This will create a grid of products in relation a category pre-selected
 */
export class ProductsListComponent {
  @Input() showSearch = false;

  @Input() set products(prods: Product[]) {
    if (prods) {
      if (!this.originalProducts) { this.originalProducts = prods; }

      // Validate if  some of the prods are in the list of selected products and replace the prods product with the selectedProducts ones
      this._products = this.storeService.selectedProducts ? prods.map(prod => {
        const selectedProd = this.storeService.selectedProducts.find(selProd => selProd.id === prod.id);
        if (selectedProd) { prod = selectedProd; }
        return prod;
      }) : prods;
    }
  }

  get products(): Product[] {
    return this._products;
  }

  public originalProducts: Product[];
  public currentSort: SortFilterEnum;
  public showAddIndex: number;
  public searchProduct: string;

  public sortValues = [
    {
      description: this.translateService.instant( 'common_price'),
      enumType: SortFilterEnum.price
    }, {
      description: this.translateService.instant( 'common_availability'),
      enumType: SortFilterEnum.available
    }, {
      description: this.translateService.instant( 'common_quantity'),
      enumType: SortFilterEnum.qty
    }
  ];
  public sortIconDir: string;
  public sortAsc: boolean;

  private _products: Product[];

  constructor(private storeService: StoreService, private translateService: TranslateService) {
    this.sortIconDir = 'arrow_upward';
    this.showAddIndex = null;
    this.searchProduct = '';
    this.sortAsc = false;
  }

  /**
   * Method to create the show index to control the products-action view
   */
  public setShowIndex(product: Product) {
    this.showAddIndex = product.id;
  }

  /**
   * Method to apply the sort selected
   */
  public setSort(sortType: SortFilterEnum, keepAsc?: boolean) {
    if (!keepAsc) {
      this.sortAsc = sortType !== this.currentSort && keepAsc ? true : !this.sortAsc;
    }

    this.currentSort = sortType;
    this.products = this.products.sort((a, b) => this.sortFn(a, b, sortType,  this.sortAsc));
  }

  /**
   * Method to apply the filter selected and call the setSort method
   */
  public applyFilter(filteredProducts: Product[]) {
    this.products = filteredProducts;

    if (this.currentSort) {
      this.setSort(this.currentSort, true);
    }
  }

  /**
   * Method to reset the sort selected
   */
  public resetFilter() {
    this.products = [...this.originalProducts];
  }

  /**
   * Method to standardize the sort function
   */
  private sortFn(a, b, index, asc) {
    const [valueA, valueB] = asc ? [a, b] : [b, a];
    return String(valueA[index]).localeCompare(String(valueB[index]), 'kn', {numeric: true});
  }
}
