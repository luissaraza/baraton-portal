import {Component, Input, ViewEncapsulation} from '@angular/core';
import {animate, style, transition, trigger} from '@angular/animations';

import { Product } from '@app/core/models/product.model';

import { StoreService } from '@app/shared/services/store/store.service';

enum UpdateQuantity {
  add,
  remove,
}

@Component({
  selector: 'app-products-action',
  templateUrl: './products-action.component.html',
  styleUrls: ['./products-action.component.scss'],
  encapsulation: ViewEncapsulation.None,
  animations: [
    trigger(
      'enterAnimation', [
        transition(':enter', [
          style({ opacity: '0'  }),
          animate('200ms', style({  opacity: '1' }))
        ]),
        transition(':leave', [
          style({  opacity: '1'  }),
          animate('200ms', style({  opacity: '0'  }))
        ])
      ]
    )
  ]
})

/**
 * This will create a component to control the quantity selected of a product
 */
export class ProductsActionComponent {
  @Input() showId?: number;
  @Input() customClass?: string;

  get product(): Product {
    return this._product;
  }

  @Input('product')
  set product(product: Product) {
    product.current_quantity = product.current_quantity ? product.current_quantity : 0;
    this._product = product;
  }

  public added: boolean;
  public updateQuantityEnum = UpdateQuantity;

  private _product: Product;

  constructor(private storeService: StoreService) {
    this.added = false;
  }

  /**
   * This method will control the update process, if add or remove quantity
   */
  public updateQuantity(action: UpdateQuantity) {
    if (this.product.available) {
      switch (action) {
        case UpdateQuantity.add:
          this.product.current_quantity += 1;
          this.product.current_price = this.calcCurrentPrice(this.product);
          this.storeService.saveProduct(this.product);
          break;
        case UpdateQuantity.remove:
          this.product.current_quantity -= 1;

          // If the current_quantity is 0 then we need to remove the product from the basket
          if (this.product.current_quantity === 0) {
            this.storeService.removeProduct(this.product);
          } else {
            this.product.current_price = this.calcCurrentPrice(this.product);
            this.storeService.saveProduct(this.product);
          }
          break;
        default:
      }
    }
  }

  /**
   * This simple method will calculate the current price of the product in relation with the real price and the current_quantity
   */
  public calcCurrentPrice(product: Product): number {
    return product.formattedPrice * product.current_quantity;
  }
}
