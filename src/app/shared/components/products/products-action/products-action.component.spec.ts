import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ProductsActionComponent } from './products-action.component';

describe('ProductsActionComponent', () => {
  let component: ProductsActionComponent;
  let fixture: ComponentFixture<ProductsActionComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ProductsActionComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ProductsActionComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
