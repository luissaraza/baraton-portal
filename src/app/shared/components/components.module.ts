import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';
import { FormsModule } from '@angular/forms';

import { MaterialModule } from '@app/material.module';

import { CategoriesListService } from './categories-list/categories-list.service';

import { PipesModule } from '@app/shared/pipes/pipes.module';

import { CategoriesListComponent } from './categories-list/categories-list.component';
import { ProductsListComponent } from './products/products-list/products-list.component';
import { SpinnerComponent } from './spinner/spinner.component';
import { ProductsActionComponent } from './products/products-action/products-action.component';
import { ShoppingCartComponent } from './shopping-cart/shopping-cart.component';
import { ImageLazyLoadComponent} from '@app/shared/components/image-lazy-load/image-lazy-load.component';
import { FilterProductsComponent } from './filter-products/filter-products.component';
import { RangeFilterComponent } from './filter-products/range-filter/range-filter.component';
import {TranslateModule} from '@ngx-translate/core';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,

    MaterialModule,
    PipesModule,
    RouterModule,

    TranslateModule.forRoot()
  ],
  declarations: [
    CategoriesListComponent,
    ProductsListComponent,
    SpinnerComponent,
    ProductsActionComponent,
    ShoppingCartComponent,
    ImageLazyLoadComponent,
    FilterProductsComponent,
    RangeFilterComponent,

  ],
  providers: [
    CategoriesListService,
  ],
  exports: [
    CategoriesListComponent,
    ProductsListComponent,
    SpinnerComponent,
    ProductsActionComponent,
    ShoppingCartComponent,
    ImageLazyLoadComponent,
    FilterProductsComponent,
    RangeFilterComponent
  ]
})
export class ComponentsModule { }
