import { Component, Input } from '@angular/core';

@Component({
  selector: 'app-spinner',
  templateUrl: './spinner.component.html',
  styleUrls: ['./spinner.component.scss']
})

/**
 * This simple component will control a spinner with a basic text, to standardize the use trough all the web page
 */
export class SpinnerComponent {
  @Input() loadingText = '';

  constructor() { }
}
