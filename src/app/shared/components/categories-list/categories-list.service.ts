import { Injectable } from '@angular/core';

import { BehaviorSubject } from 'rxjs';

@Injectable({
  providedIn: 'root'
})

/**
 * This service will allow to control the toggle navbar categories from outside
 */
export class CategoriesListService {

  // Observable string source
  public dataSubject$: BehaviorSubject<boolean> = new BehaviorSubject<boolean>(false);
  private currentViewState = false;

  constructor() { }

  /**
   * Method to toggle the categories list navbar from outside
   */
  toggle(state?: boolean) {
    this.currentViewState = state || !this.currentViewState;
    this.dataSubject$.next(this.currentViewState);
  }

}
