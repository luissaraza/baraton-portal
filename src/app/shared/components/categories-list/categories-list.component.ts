import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import { Category } from '@app/core/models/category.model';
import {StoreService} from '@app/shared/services/store/store.service';
import {animate, state, style, transition, trigger} from '@angular/animations';
import {Observable} from 'rxjs';
import {CategoriesListService} from '@app/shared/components/categories-list/categories-list.service';

@Component({
  selector: 'app-categories-list',
  templateUrl: './categories-list.component.html',
  styleUrls: ['./categories-list.component.scss'],
  animations: [
    trigger('showHideAnimation', [
      state('show', style({
        height: '*',
      })),
      state('hide', style({
        height: '0',
        pointerEvents: 'none'
      })),
      transition('hide => show', animate('300ms ease')),
      transition('show => hide', animate('300ms ease')),
    ]),
  ],
})

/**
 * This component will build a categories tree, iterating over a categories a array and creating a tree of them dynamically
 */
export class CategoriesListComponent implements OnInit {
  @Input() categories: Category[] = [];
  @Output() selected ? = new EventEmitter();

  public currentCategory: Category;
  public collapsed: Observable<boolean>;

  constructor(private storeService: StoreService, private  categoriesListService: CategoriesListService) {
    this.currentCategory = null;
  }

  ngOnInit() {
    this.collapsed = this.categoriesListService.dataSubject$;
  }

  /**
   * Method to emit the current category selected
   */
  public selectCategory(cat: Category) {
    this.currentCategory = cat;
    this.selected.emit(cat);
  }

  /**
   * Method to toggle the category navbar and close other if its a parent category
   */
  public toggleCategory(cat: Category, isParent: boolean) {
    // If select a parent category, then close all categories
    if (isParent) {
      this.categories.map(category => {
        if (cat !== category) {
          category.isExpanded = false;
        }

        return category;
      });
    }

    // Toggle category expanded property
    cat.isExpanded = !cat.isExpanded;
  }


  /**
   * Method to close the categories menu navbar
   */
  public closeMenu() {
    this.categoriesListService.toggle(false);
  }

}
