import { NgModule } from '@angular/core';
import { TranslateModule } from '@ngx-translate/core';

import { CommonModule } from '@angular/common';
import { ComponentsModule } from './components/components.module';
import { ServicesModule } from './services/services.module';
import {PipesModule} from '@app/shared/pipes/pipes.module';

@NgModule({
  imports: [
    CommonModule,

    TranslateModule,
    ServicesModule,
    PipesModule,
    ComponentsModule
  ],
  declarations: [],
  exports: [
    CommonModule,

    TranslateModule,
    ServicesModule,
    PipesModule,
    ComponentsModule
  ],
  providers: []
})
export class SharedModule { }
