import { Injectable } from '@angular/core';
import idb, { DB } from 'idb';

/**
 * This class will create a database providing a set of functions to control this one
 */
export class DataBase {
  dbName: string;
  dbVersion: number;
  db: Promise<DB>;

  constructor(dbName: string, version: number, cb?: any) {
    this.dbName = dbName;
    this.dbVersion = version || 1;

    this.db = idb.open(this.dbName, this.dbVersion, cb);
  }

  /**
   * This method will get the value of a store with specific key
   */
  get(storeName: string, key: string) {
    return this.db.then((db: DB) => {
      return db.transaction(storeName)
        .objectStore(storeName).get(key);
    });
  }

  /**
   * This method will get tall the values of specific store, you can pass a noKeys to avoid get those specific keys
   */
  getAll(storeName: string, noKeys: boolean = false) {
    return this.db.then((db: DB) => {
      const keys = {};
      const tx = db.transaction(storeName);
      const store = tx.objectStore(storeName);

      if (noKeys) {
        return store.getAll();
      } else {
        // This would be store.getAllKeys(), but it isn't supported by Edge or Safari.
        // openKeyCursor isn't supported by Safari, so we fall back
        (store.iterateKeyCursor || store.iterateCursor).call(store, cursor => {
          if (!cursor) { return; }
          store.get(cursor.key).then(val =>  keys[cursor.key] = val);
          cursor.continue();
        });

        return tx.complete.then(() => keys);
      }
    });
  }

  /**
   * This method will set the value of a store with specific key
   */
  set(storeName: string, key: string, val: any) {
    return this.db.then((db: DB) => {
      const tx = db.transaction(storeName, 'readwrite');
      tx.objectStore(storeName).put(val, key);
      return tx.complete;
    });
  }

  /**
   * This method will delete the value of a store with specific key
   */
  delete(storeName: string, key: string) {
    return this.db.then((db: DB) => {
      const tx = db.transaction(storeName, 'readwrite');
      tx.objectStore(storeName).delete(key);
      return tx.complete;
    });
  }

  /**
   * This method will get the keys of specific store
   */
  keys(storeName: string) {
    return this.db.then((db: DB) => {
      const tx = db.transaction(storeName);
      const keys = [];
      const store = tx.objectStore(storeName);

      // This would be store.getAllKeys(), but it isn't supported by Edge or Safari.
      // openKeyCursor isn't supported by Safari, so we fall back
      (store.iterateKeyCursor || store.iterateCursor).call(store, cursor => {
        if (!cursor) { return; }
        keys.push(cursor.key);
        cursor.continue();
      });

      return tx.complete.then(() => keys);
    });
  }
}

@Injectable({
  providedIn: 'root'
})
/**
 *  This Service will simplify the use of index DB
 */
export class IndexDbService {
  /**
   * Open the database with some params passed trough
   */
  openDB(dbName: string, version?: number, upgradeDB?: any): DataBase {
    return new DataBase(dbName, version, upgradeDB);
  }
}
