import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

import { environment } from '@env/environment';

import { Category} from '@app/core/models/category.model';
import { Product, UpdatedProduct } from '@app/core/models/product.model';
import { CategoryProducts } from '@app/core/models/categoryProduct.model';

import { DataBase, IndexDbService } from '@app/shared/services/indexDB/index-db.service';

import { BehaviorSubject, Observable } from 'rxjs';
import { map } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})

/**
 * This service will control al the store requests (Due the size of the company)
 */
export class StoreService {
  private updatedProducts$: BehaviorSubject<UpdatedProduct> = new BehaviorSubject<UpdatedProduct>(null);
  private _currentProducts: Product[] = [];
  private portalDB: DataBase;
  private _cachedCategoriesProduct: CategoryProducts[];

  constructor(private http: HttpClient, private indexDbService: IndexDbService) {
    this._cachedCategoriesProduct = [];
    this._currentProducts = [];

    // Open the PortalBaraton DB
    this.portalDB = this.indexDbService.openDB('PortalBaraton');
    this.portalDB
      .get('category-products', 'products')
      .then((prods: Product[]) => {
        // If we have products into  index DB then launch an update to all that is listening updatedProducts$ subject
        this._currentProducts = prods || [];
        this.launchUpdate(this._currentProducts, null, false);
      });

  }

  get onUpdateProduct$ (): BehaviorSubject<UpdatedProduct> {
    return this.updatedProducts$;
  }

  get loadedCategories (): CategoryProducts[] {
    return this._cachedCategoriesProduct;
  }

  get selectedProducts (): Product[] {
    return this._currentProducts;
  }

  /**
   * Method to get the system Categories
   */
  getCategories(): Observable<Category[]> {
    return <Observable<Category[]>>this.http.get(`${environment.serverUrl}/categories`);
  }

  /**
   * Methos to get the products from specific Category (NO use of backend side, then we need to map the products to simulate an SQL search)
   */
  getProducts(cat?: Category): Observable<Product[]> {
    return <Observable<Product[]>> this.http.get(`${environment.serverUrl}/products`).pipe(
      map((products: Product[]) =>  {
        const filteredProducts = products
          .map(pro => {
            pro.formattedPrice = this.unformatPrice(pro.price);
            return pro;
          })
          .filter(pro => pro.sublevel_id === cat.id);

        // Save the loaded categories with its correspondent products
        this._cachedCategoriesProduct.push({...cat, products: filteredProducts});

        return filteredProducts;
      }),
    );
  }

  /**
   * Method to save a product into the selected products and launch an update
   */
  saveProduct (product: Product) {
    if (!this._currentProducts.includes(product)) {
      this._currentProducts.push(product);
    }
    this.launchUpdate(this._currentProducts, product, true);
  }

  /**
   * Method to remove a product from the selected products and launch an update
   */
  removeProduct (product: Product) {
    this.resetProduct(product);
    this._currentProducts = this._currentProducts.filter(pro => pro.id !== product.id);
    this.launchUpdate(this._currentProducts, product, false);
  }

  /**
   * Mehot to clean all the order (Reset and remove all products)
   */
  cleanOrder() {
    // Clean products
    this._currentProducts.map(pro => this.resetProduct(pro));
    this._currentProducts = [];
    this.launchUpdate(this._currentProducts, null, false);

    // Delete the products key from category products store
    this.portalDB.delete('category-products', 'products');
  }

  /**
   * Method to simulate a confirm order instruction
   */
  confirmOrderFake (fakeError?: boolean) {
    return new Promise((resolve, reject) => {
      setTimeout(() => {
        fakeError ? reject() : resolve();
      }, 5000);
    });
  }

  // Method to un-format the original price to number
  private unformatPrice(price: string): number {
    return Number(price.replace('$', '').replace(',', ''));
  }

  /**
   * Method to reset a product as new
   */
  private resetProduct(pro: Product) {
    pro.current_quantity = 0;
    pro.current_price = 0;
  }

  /**
   * This method will launch the watchers of updatedProducts$ subjects
   */
  private launchUpdate(prods: Product[], last: Product, add: boolean) {
    this.updatedProducts$.next({
      products: prods,
      last: last,
      add: add
    });
  }
}
