import {NgModule, Optional, SkipSelf} from '@angular/core';
import { CommonModule } from '@angular/common';
import { StoreService } from './store/store.service';
import { I18nService } from './i18n/i18n.service';
import {IndexDbService} from './indexDB/index-db.service';

@NgModule({
  imports: [
    CommonModule,
  ],
  declarations: [],
  providers: [
    I18nService,
    StoreService,
    IndexDbService
  ],
  exports: []
})
export class ServicesModule {
  constructor(@Optional() @SkipSelf() parentModule: ServicesModule) {
    // Import guard
    if (parentModule) {
      throw new Error(`${parentModule} has already been loaded. Import Core module in the AppModule only.`);
    }
  }
}
