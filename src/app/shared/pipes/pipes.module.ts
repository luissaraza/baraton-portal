import { NgModule} from '@angular/core';
import { CommonModule } from '@angular/common';
import { FilterForPipe } from './filter-for/filter-for.pipe';

@NgModule({
  imports: [
    CommonModule,
  ],
  declarations: [
    FilterForPipe
  ],
  providers: [],
  exports: [
    FilterForPipe
  ]
})
export class PipesModule {
  constructor() {}
}
