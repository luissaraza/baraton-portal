import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'filterFor'
})

/**
 * This pipe will filter a set of objects from array with some specifications
 * example: *ngFor="let product of products | filterFor : searchProduct : 'name'"
 */
export class FilterForPipe implements PipeTransform {
  transform(items: any[], searchText: string, index: string, disableFilter: boolean): any[] {
    if (!items) { return []; }
    if (!searchText || disableFilter) { return items; }

    searchText = searchText.toLowerCase();

    return items.filter((item: any) => {
      const itemCompare = index ? item[index] : item;
      return itemCompare.toLowerCase().includes(searchText);
    });
  }
}
