import { Component, OnInit } from '@angular/core';
import { environment } from '@env/environment';
import { I18nService } from '@app/shared/services/i18n/i18n.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent implements OnInit {

  constructor(private i18nService: I18nService) {}

  ngOnInit() {
    // Setup translations
    this.i18nService.init(environment.defaultLanguage, environment.supportedLanguages);
  }
}
