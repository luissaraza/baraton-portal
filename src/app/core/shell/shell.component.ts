import { Component } from '@angular/core';

@Component({
  selector: 'app-shell',
  templateUrl: './shell.component.html',
  styleUrls: ['./shell.component.scss']
})

/**
 * Will be the main component trough the web page
 */
export class ShellComponent  {
  constructor() {}
}
