import { Component } from '@angular/core';
import { CategoriesListService } from '@app/shared/components/categories-list/categories-list.service';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss']
})

/**
 * Default header that will be visible trough all the web page
 */
export class HeaderComponent {

  constructor(private categoriesListService: CategoriesListService) {}

  /**
   * This method will toggle the navbar categories menu
   */
  actionHamMenu() {
    this.categoriesListService.toggle();
  }
}
