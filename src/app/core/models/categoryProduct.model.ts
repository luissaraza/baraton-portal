import { Category } from '@app/core/models/category.model';
import { Product } from '@app/core/models/product.model';

export interface CategoryProducts extends Category {
  products: Product[];
}
