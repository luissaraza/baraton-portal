export interface Product {
  quantity: number;
  price: string;
  formattedPrice: number;
  available: boolean;
  sublevel_id: 11;
  name: string;
  id: number;
  current_quantity?: number;
  current_price?: number;
}

export interface UpdatedProduct {
  products: Product[];
  last: Product;
  add: boolean;
}
