export interface Filter {
  available: boolean;
  priceRange: MinMax;
  qtyRange: MinMax;
}

export interface MinMax {
  min: number;
  max: number;
}
