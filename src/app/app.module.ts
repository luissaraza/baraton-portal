import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

// Own pages
import { AppComponent } from '@app/app.component';
import { AppRoutingModule } from '@app/app-routing.module';
import { FormsModule } from '@angular/forms';
import { CoreModule } from '@app/core/core.module';
import { SharedModule } from '@app/shared/shared.module';
import { TranslateModule } from '@ngx-translate/core';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { PagesModule } from '@app/pages/pages.module';
import {IndexDbService} from '@app/shared/services/indexDB/index-db.service';
import {UpgradeDB} from 'idb';

@NgModule({

  imports: [
    BrowserModule,
    FormsModule,
    CoreModule,
    SharedModule,

    PagesModule,

    AppRoutingModule,

    BrowserAnimationsModule,
    TranslateModule.forRoot()
  ],
  exports: [],
  providers: [],
  declarations: [AppComponent],
  bootstrap: [AppComponent]
})
export class AppModule {


  // I initialize the module.
  constructor( private indexDBService: IndexDbService ) {
    this.indexDBService.openDB('PortalBaraton', 1, (upgradeDB: UpgradeDB) => {
      switch (upgradeDB.oldVersion) {
        case 0:
        // a placeholder case so that the switch block will
        // execute when the database is first created
        // (oldVersion is 0)
        case 1:
          upgradeDB.createObjectStore('category-products');
          break;
        default:
      }
    });
  }
}
